from flask import Flask, request
from flask_restful import Api, Resource
from pymongo import MongoClient
import bcrypt

app = Flask(__name__)
api = Api(app)
client = MongoClient("mongodb://db:27017")

db = client.testDB
user = db["User"]


def verifyUser(username, password):
    userData = userGet(username)
    if userData.count():
        if bcrypt.hashpw(password.encode('utf8'),
                         userData[0]["Passwd"]) == userData[0]["Passwd"]:
            return True
        else:
            return False
    else:
        return False


def countToken(username):
    userData = userGet(username)
    if userData.count():
        return userData[0]["Token"]
    else:
        return 0


def userExist(username):
    return user.find({"UserName": username}).count()


def userGet(username):
    return user.find({"UserName": username})


def retriveSentence(username):
    userdata = userGet(username)
    if userdata.count() and userdata[0]["Sentence"]:
        return True

    else:
        return False


class Register(Resource):
    def post(self):
        postedData = request.get_json()
        username = postedData["username"]
        password = postedData["password"]
        checkuser = userExist(username)  # it return count 0 or 1
        if checkuser:
            return {"Statuscode": 403, "msg": "User already exist"}, 403
        else:
            hashed_pw = bcrypt.hashpw(password.encode('utf8'),
                                      bcrypt.gensalt())
            user.insert_one({
                "UserName": username,
                "Passwd": hashed_pw,
                "Sentence": "",
                "Token": 5
            })
            return {"Statuscode": 201, "msg": "successful registration"}, 201


class Store(Resource):
    def post(self):
        postedData = request.get_json()
        username = postedData["username"]
        password = postedData["password"]
        sentence = postedData["sentences"]
        # 1.verify user
        # verify token
        checkUser = verifyUser(username, password)
        if not checkUser:
            return {"status": 401, "msg": "Invalid username or password"}, 401
        num_tokens = countToken(username)
        if num_tokens <= 0:
            return {"status": 403, "msz": "user don't have enough token"}, 403

        user.update_one(
            {"UserName": username},
            {"$set": {
                "Sentence": sentence,
                "Token": num_tokens - 1
            }})
        return {
            "status": 200,
            "msz": "sentence suceesful store",
            "availbletoken": countToken(username)
        }


class Retrive(Resource):
    def post(self):
        postedData = request.get_json()
        username = postedData["username"]
        password = postedData["password"]
        # reture true if exist or return false if not exist
        checkUser = verifyUser(username, password)
        if not checkUser:
            return {"status": 401, "msg": "Invalid username or password"}, 401
        num_tokens = countToken(username)

        if num_tokens <= 0:
            return {"status": 403, "msz": "user don't have enough token"}, 403

        retrivesentence = retriveSentence(username)
        if not retrivesentence:
            return {"status": 403, "msz": "Sentences is empty"}, 403

        user.update_one(
            {"UserName": username},
            {"$set": {
                "Token": num_tokens - 1
            }})
        userdata = userGet(username)
        return {
            "status": 200,
            "sentences": userdata[0]["Sentence"],
            "availbletoken": userdata[0]["Token"]
        }, 200


api.add_resource(Register, '/register')
api.add_resource(Store, '/store')
api.add_resource(Retrive, '/retrive')

if __name__ == "__main__":
    app.run(host="0.0.0.0", debug=True)

# if bcrypt.hashpw(password, hashed) == hashed:
#         print "It matches"
# else:
#         print "It does not match"
